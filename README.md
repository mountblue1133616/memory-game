# Memory Game-KUNGFU PANDA!

This is a Memory Game web application built with HTML, CSS, and JavaScript. The game presents a grid of cards facedown, and the objective is to find all matching pairs by flipping them over two at a time.
Demo Link: https://wondrous-baklava-512f60.netlify.app/

![Memory Game Screenshot](public/screenshot.jpg)

## Features

- Grid of cards dynamically generated based on a set of predefined data.
- Timer that starts when the first card is flipped and stops when all matches are found.
- Moves counter to keep track of the number of moves made.
- Game-over state displayed when all matches are found.
- Responsive design for optimal gameplay on different devices.

## Technologies Used

- HTML5
- CSS3
- JavaScript

## Setup and Usage

1. Clone the repository or download the source code.

   ```
   git clone https://github.com/mountblue1133616/memory-game.git
   ```

2. Open the `index.html` file in a web browser.

3. The game will start automatically. Click on any card to flip it over.

4. Try to find all matching pairs by flipping two cards at a time. If the cards match, they will remain face up. If they don't match, they will be flipped back face down.

5. The timer will start when the first card is flipped, and the moves counter will update with each pair of cards flipped.

6. Keep playing until all matches are found. When all matches are found, a game-over state will be displayed, showing the final time and number of moves.

## Customization

You can customize the game by modifying the `dataArray` array in the JavaScript code. The array contains objects representing each card, where each object has a `name` and `imagePath`. You can add or remove objects to change the cards in the game.

## Acknowledgments

The Memory Game is inspired by the classic memory matching game and was developed as a practice project.  
Resources: https://www.taniarascia.com/how-to-create-a-memory-game-super-mario-with-plain-javascript/
