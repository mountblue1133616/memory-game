const dataArray = [
  {
    name: "po",
    imagePath: "public/Po.jpeg",
  },
  {
    name: "crane",
    imagePath: "public/Crane.jpeg",
  },
  {
    name: "ping",
    imagePath: "public/Ping.png",
  },
  {
    name: "oogway",
    imagePath: "public/Monkey.jpeg",
  },
  {
    name: "shifu",
    imagePath: "public/Shifu.jpeg",
  },
  {
    name: "tailung",
    imagePath: "public/TaiLung.jpeg",
  },
  {
    name: "tigress",
    imagePath: "public/Tigress.jpeg",
  },
  {
    name: "viper",
    imagePath: "public/Viper.png",
  },
];

let game = document.querySelector(".game");
let start = document.querySelector("#start");
let stop = document.querySelector("#stop");
let parent = document.querySelector(".parent-container");
let grid = document.createElement("section");
grid.setAttribute("class", "grid");
parent.appendChild(grid);
let count = 0;
let timerStart = false;
let moves = 0;
let firstGuess = "";
let secondGuess = "";
let previousTarget = null;
let timerInterval = null;
let timer = 0;
let delay = 1200;

let gameGrid = dataArray.concat(dataArray);
gameGrid.sort(() => 0.5 - Math.random());

gameGrid.forEach((item) => {
  const card = document.createElement("div");
  card.classList.add("card");
  card.dataset.name = item.name;
  const front = document.createElement("div");
  front.classList.add("front");
  const back = document.createElement("div");
  back.classList.add("back");
  back.style.backgroundImage = `url(${item.imagePath})`;
  grid.appendChild(card);
  card.appendChild(front);
  card.appendChild(back);
});

const startTimer = () => {
  timerStart = true;
  timerInterval = setInterval(() => {
    timer++;
    const minutes = Math.floor(timer / 60);
    const seconds = timer % 60;
    const formattedTime = `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
    document.getElementById("timer").textContent = `Time: ${formattedTime}`;
  }, 1000);
};

const stopTimer = () => {
  timer = 0;
  timerStart = false;
  clearInterval(timerInterval);
};

const updateMoves = () => {
  moves++;
  document.getElementById("moves").textContent = `${moves} Moves`;
};

const match = () => {
  var selected = document.querySelectorAll(".selected");
  selected.forEach((card) => {
    card.classList.add("match");
  });
  const matchedCards = document.querySelectorAll(".match");
  console.log(matchedCards.length);
  console.log(gameGrid.length);
  if (matchedCards.length == gameGrid.length) {
    stopTimer();
    gameGrid.sort(() => 0.5 - Math.random());
    game.classList.remove("none");
    game.classList.add("game-over");
  }
};

const resetGuesses = () => {
  firstGuess = "";
  secondGuess = "";
  count = 0;

  var selected = document.querySelectorAll(".selected");
  selected.forEach((card) => {
    updateMoves();
    card.classList.remove("selected");
  });
};

const resetMatches = () => {
  var matches = document.querySelectorAll(".match");
  matches.forEach((card) => {
    card.classList.remove("match");
  });
};

grid.addEventListener("click", (event) => {
  if (!timerStart) {
    return;
  }
  let clicked = event.target;
  if (
    clicked.nodeName === "SECTION" ||
    clicked == previousTarget ||
    clicked.parentNode.classList.contains("selected") ||
    clicked.parentNode.classList.contains("match")
  ) {
    return;
  }

  if (count < 2) {
    count++;
    if (count === 1) {
      firstGuess = clicked.parentNode.dataset.name;
      clicked.parentNode.classList.add("selected");
    } else {
      secondGuess = clicked.parentNode.dataset.name;
      console.log(secondGuess);
      clicked.parentNode.classList.add("selected");
    }
    if (firstGuess !== "" && secondGuess !== "") {
      if (firstGuess === secondGuess) {
        setTimeout(match, delay);
        setTimeout(resetGuesses, delay);
      } else {
        setTimeout(resetGuesses, delay);
      }
    }
    previousTarget = clicked;
  }
});

start.addEventListener("click", (event) => {
  if (!timerStart) {
    startTimer();
  }
});

stop.addEventListener("click", (event) => {
  timer = 0;
  document.getElementById("timer").textContent = `Time: 00:00`;
  stopTimer();
  resetGuesses();
  resetMatches();
  moves = 0;
  document.getElementById("moves").textContent = `0 Moves`;
});
